var div_original = document.getElementById('div');
//MOSTRA A LISTA COMPLETA DE BD
function lista(){

    div_original.innerHTML="";

  fetch("https://treinamentoajax.herokuapp.com/messages")
  .then(response  => response.json())
  .then(response =>{

      for(i in response){
        var divi = document.createElement('div');
        divi.innerHTML = `<h1> ${response[i].name} </h1>
        <p> ${response[i].message}<\p>
        <div id=${response[i].id}> </div>
        <button value=${response[i].id} class='Deleta' onclick="deletar(value)">Deletar</button>
        <button value=${response[i].id} class='Deleta' onclick="edit(${response[i].id})">Editar Mensagem</button>`
        div_original.appendChild(divi);
       }
  })
  .catch(response => console.warn(response));
}

function edit(id){
      
    var outra_div = document.createElement('div');
    outra_div.innerHTML=` <p>Nome:<\p>
    <input type="text" placeholder="Digite Seu Nome" id="input1"></input>
    <p>Mensagem:</p>
    <input type="text" placeholder="Digite Sua Mensagem"  id="input2"></input>
    <button onclick="atualiza(${id})">Editar</button>
    <button onclick="lista()">Cancelar</button>`
    var div = document.getElementById(id);
    div.appendChild(outra_div);
}

//Olhar com calma, mexi em nada por enquanto
function atualiza(id){

    var user = document.getElementById('input1');
    var message = document.getElementById('input2');
    console.log(id);
    if(user.value=="" && message.value==""){
        alert("Nenhum valor inserido");
    }
    if(!(user.value=="") && message.value==""){

        let fetchBody={
            "message":{
                "name":`${user.value}`
            }
        }

        let fetchConfig={
            method:"PUT",
            headers:{"Content-Type": "application/JSON"},
            body: JSON.stringify(fetchBody)
        }

        fetch(`https://treinamentoajax.herokuapp.com/messages/${id}`, fetchConfig)
        .then(response => response.json())
        .then(alert("Nome Alterado"))

    }

    if(user.value=="" && !(message.value=="")){
        let fetchBody={
            "message":{
                "message":`${message.value}`
            }
        }

        let fetchConfig={
            method:"PUT",
            headers:{"Content-Type": "application/JSON"},
            body: JSON.stringify(fetchBody)
        }

        fetch(`https://treinamentoajax.herokuapp.com/messages/${id}`, fetchConfig)
        .then(response => response.json())
        .then(alert("Mensagem Alterado"))

    }
    if(!(user.value=="") && !(message.value=="")){


        let fetchBody={
            "message":{
                "name":`${user.value}`,
                "message":`${message.value}`
            }
        }

        let fetchConfig={
            method:"PUT",
            headers:{"Content-Type": "application/JSON"},
            body: JSON.stringify(fetchBody)
        }

        fetch(`https://treinamentoajax.herokuapp.com/messages/${id}`, fetchConfig)
        .then(response => response.json())
        .then(alert("Nome e Mensagem Alterado"))
    }
    especifica_outro(id);

}


function deletar(id){
    fetch(`https://treinamentoajax.herokuapp.com/messages/${id}`, {
     method:"DELETE"
 }).then(alert("Mensagem Apagada com Sucesso"))
 
}


function dom(){
    var name = document.getElementById('name');
    name = name.value;
    var message = document.getElementById('message');
    message = message.value;

    const fetchBody = {
      "message":{
         "name": `${name}`,
          "message": `${message}`
        }
    }
    const fetchConfig = {
        method: "POST",
        headers: {"Content-Type":"application/JSON"},
        body: JSON.stringify(fetchBody)
    }

    fetch("https://treinamentoajax.herokuapp.com/messages", fetchConfig)
    .then(console.log)
    .then(alert("Nome e Mensagem enviada ao Banco de Dados"))
    .catch(response => console.warn(response));
}


//Especifica
function especifica(){
    var especifico = document.getElementById('get-2');
    fetch(`https://treinamentoajax.herokuapp.com/messages/${especifico.value}`)
 .then(response => response.json())
 .then(response => {
     var divi = document.createElement('div');
     divi.innerHTML = `<h1> ${response.name} </h1>
     <p> ${response.message}<\p>
     <div id=${especifico.value}> </div>
     <button value=${especifico.value} class='Deleta' onclick="deletar(value)">Deletar</button>
     <button value=${especifico.value} class='Deleta' onclick="edit(${especifico.value})">Editar Mensagem</button>`
     div_original.innerHTML="";
     div_original.appendChild(divi);
    })
}

function especifica_outro(id){
    fetch(`https://treinamentoajax.herokuapp.com/messages/${id}`)
 .then(response => response.json())
 .then(response => {
     var divi = document.createElement('div');
     divi.innerHTML = `<h1> ${response.name} </h1>
     <p> ${response.message}<\p>
     <div id=${id}> </div>
     <button value=${id} class='Deleta' onclick="deletar(value)">Deletar</button>
     <button value=${id} class='Deleta' onclick="edit(${id})">Editar Mensagem</button>`
     div_original.innerHTML="";
     div_original.appendChild(divi);
    })
}
